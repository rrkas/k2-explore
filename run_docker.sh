docker run \
    -it \
    --gpus all \
    -v .:/workspace \
    pytorch/pytorch:2.2.1-cuda12.1-cudnn8-devel \
    bash
