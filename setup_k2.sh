apt-get update -y && \
    apt-get upgrade -y && \
    apt update -y && \
    apt upgrade -y

apt-get install -y git

pip install pip-chill

pip-chill > requirements.txt

cd /workspace

git clone https://github.com/k2-fsa/k2.git && \
    cd k2 && \
    echo 'export K2_MAKE_ARGS="-j6"' >> ~/.bashrc && \
    bash -c "source ~/.bashrc" && \
    python3 setup.py install

cd /workspace

pip install git+https://github.com/lhotse-speech/lhotse

cd /workspace

git clone https://github.com/k2-fsa/icefall && \
    cd icefall && \
    pip install -r requirements.txt && \
    echo 'export PYTHONPATH=/workspace/icefall:$PYTHONPATH' >> ~/.bashrc && \
    bash -c "source ~/.bashrc"


conda install gcc=12.1.0
conda install -c conda-forge gcc=12.1.0



